### MR Sales [APT Monthly Report]

**Read data from Excel binary file**


1. import pyxlsb
2. import convert_date to convert column number
3. read data from sheets: "**Step 1 - Product Info**", "**Sales**"
4. with Data from use **drop([columns], axis = 1)** to remove columns have different with "**Real (M)**"
5. read n rows by **join 2 DataFrames **
6. insert n rows into database
