import io
import os
import sys
sys.path.append(os.path.realpath('.'))
import json
from dateutil import parser
from info.sharepoint import server, user, password, sites_name, folder, file_name, folder_budget, file_name_budget
import sharepy
from sharepy import connect
from connect.database import connect

class connect_with_sharepy():
    def __init__(self):
        pass

    def sharepoint(self):
        conn = sharepy.connect(server, user, password)

        return conn


class last_modified_actual_forecast():
    def __init__(self):
        pass

    def framasDE(self):
        site = sites_name+'/'+folder().framasDE()+'/'+file_name().framasDE()
        r = conn.get(site, headers=headers)
        f = io.BytesIO(r.content)
        data = str(r.headers)
        data = data.replace("\"","\<><>")
        data = data.replace("\'","\"")
        data = data.replace("\<><>","\'")
        data = json.loads(data)
        last_modified = data['Last-Modified']
        last_modified = parser.parse(last_modified)
        last_modified = last_modified.replace(tzinfo=None)
        last_modified = str(last_modified)
        
        return last_modified

    def framasFZ(self):
        site = sites_name+'/'+folder().framasFZ()+'/'+file_name().framasFZ()
        r = conn.get(site, headers=headers)
        f = io.BytesIO(r.content)
        data = str(r.headers)
        data = data.replace("\"","\<><>")
        data = data.replace("\'","\"")
        data = data.replace("\<><>","\'")
        data = json.loads(data)
        last_modified = data['Last-Modified']
        last_modified = parser.parse(last_modified)
        last_modified = last_modified.replace(tzinfo=None)
        last_modified = str(last_modified)
        
        return last_modified

    def framasHN(self):
        site = sites_name+'/'+folder().framasHN()+'/'+file_name().framasHN()
        r = conn.get(site, headers=headers)
        f = io.BytesIO(r.content)
        data = str(r.headers)
        data = data.replace("\"","\<><>")
        data = data.replace("\'","\"")
        data = data.replace("\<><>","\'")
        data = json.loads(data)
        last_modified = data['Last-Modified']
        last_modified = parser.parse(last_modified)
        last_modified = last_modified.replace(tzinfo=None)
        last_modified = str(last_modified)
        
        return last_modified

    def framasID(self):
        site = sites_name+'/'+folder().framasID()+'/'+file_name().framasID()
        r = conn.get(site, headers=headers)
        f = io.BytesIO(r.content)
        data = str(r.headers)
        data = data.replace("\"","\<><>")
        data = data.replace("\'","\"")
        data = data.replace("\<><>","\'")
        data = json.loads(data)
        last_modified = data['Last-Modified']
        last_modified = parser.parse(last_modified)
        last_modified = last_modified.replace(tzinfo=None)
        last_modified = str(last_modified)
        
        return last_modified

    def framasKR(self):
        site = sites_name+'/'+folder().framasKR()+'/'+file_name().framasKR()
        r = conn.get(site, headers=headers)
        f = io.BytesIO(r.content)
        data = str(r.headers)
        data = data.replace("\"","\<><>")
        data = data.replace("\'","\"")
        data = data.replace("\<><>","\'")
        data = json.loads(data)
        last_modified = data['Last-Modified']
        last_modified = parser.parse(last_modified)
        last_modified = last_modified.replace(tzinfo=None)
        last_modified = str(last_modified)
        
        return last_modified

    def framasKV(self):
        site = sites_name+'/'+folder().framasKV()+'/'+file_name().framasKV()
        r = conn.get(site, headers=headers)
        f = io.BytesIO(r.content)
        data = str(r.headers)
        data = data.replace("\"","\<><>")
        data = data.replace("\'","\"")
        data = data.replace("\<><>","\'")
        data = json.loads(data)
        last_modified = data['Last-Modified']
        last_modified = parser.parse(last_modified)
        last_modified = last_modified.replace(tzinfo=None)
        last_modified = str(last_modified)
        
        return last_modified

    def framasVN(self):
        site = sites_name+'/'+folder().framasVN()+'/'+file_name().framasVN()
        r = conn.get(site, headers=headers)
        f = io.BytesIO(r.content)
        data = str(r.headers)
        data = data.replace("\"","\<><>")
        data = data.replace("\'","\"")
        data = data.replace("\<><>","\'")
        data = json.loads(data)
        last_modified = data['Last-Modified']
        last_modified = parser.parse(last_modified)
        last_modified = last_modified.replace(tzinfo=None)
        last_modified = str(last_modified)
        
        return last_modified

class previous_modified_actual_forecast():
    def __init__(self):
        pass

    def framasDE(self):
        cursor.execute('''  EXEC sp_get_last_modified_file 11, 'LAST_MODIFIED_TIME'
                        ''')
        results = cursor.fetchone()
        for row in results:
            time = row

        return time

    def framasFZ(self):
        cursor.execute('''  EXEC sp_get_last_modified_file 55, 'LAST_MODIFIED_TIME'
                        ''')
        results = cursor.fetchone()
        for row in results:
            time = row

        return time

    def framasHN(self):
        cursor.execute('''  EXEC sp_get_last_modified_file 66, 'LAST_MODIFIED_TIME'
                        ''')
        results = cursor.fetchone()
        for row in results:
            time = row

        return time

    def framasID(self):
        cursor.execute('''  EXEC sp_get_last_modified_file 31, 'LAST_MODIFIED_TIME'
                        ''')
        results = cursor.fetchone()
        for row in results:
            time = row

        return time

    def framasKR(self):
        cursor.execute('''  EXEC sp_get_last_modified_file 21, 'LAST_MODIFIED_TIME'
                        ''')
        results = cursor.fetchone()
        for row in results:
            time = row

        return time

    def framasKV(self):
        cursor.execute('''  EXEC sp_get_last_modified_file 64, 'LAST_MODIFIED_TIME'
                        ''')
        results = cursor.fetchone()
        for row in results:
            time = row

        return time

    def framasVN(self):
        cursor.execute('''  EXEC sp_get_last_modified_file 61, 'LAST_MODIFIED_TIME'
                        ''')
        results = cursor.fetchone()
        for row in results:
            time = row

        return time

class create_new_modify_time_actual_forecast():
    def __init__(self):
        pass

    def framasDE(self):

        cursor.execute('''  EXEC sp_update_last_modified_file 11,'LAST_MODIFIED_TIME',%s
                        ''',(last_modified_actual_forecast().framasDE()))
        conn_sql.commit()

        return

    def framasFZ(self):

        cursor.execute('''  EXEC sp_update_last_modified_file 55,'LAST_MODIFIED_TIME',%s
                        ''',(last_modified_actual_forecast().framasFZ()))
        conn_sql.commit()

        return

    def framasHN(self):

        cursor.execute('''  EXEC sp_update_last_modified_file 66,'LAST_MODIFIED_TIME',%s
                        ''',(last_modified_actual_forecast().framasHN()))
        conn_sql.commit()

        return

    def framasID(self):
        
        cursor.execute('''  EXEC sp_update_last_modified_file 31,'LAST_MODIFIED_TIME',%s
                        ''',(last_modified_actual_forecast().framasID()))
        conn_sql.commit()

        return

    def framasKR(self):
        
        cursor.execute('''  EXEC sp_update_last_modified_file 21,'LAST_MODIFIED_TIME',%s
                        ''',(last_modified_actual_forecast().framasKR()))
        conn_sql.commit()

        return

    def framasKV(self):
        
        cursor.execute('''  EXEC sp_update_last_modified_file 64,'LAST_MODIFIED_TIME',%s
                        ''',(last_modified_actual_forecast().framasKV()))
        conn_sql.commit()

        return

    def framasVN(self):
        
        cursor.execute('''  EXEC sp_update_last_modified_file 61,'LAST_MODIFIED_TIME',%s
                        ''',(last_modified_actual_forecast().framasVN()))
        conn_sql.commit()

        return


class last_modified_budget():
    def __init__(self):
        pass

    def framasDE(self):
        site = sites_name+'/'+folder_budget().framasDE()+'/'+file_name_budget().framasDE()
        r = conn.get(site, headers=headers)
        f = io.BytesIO(r.content)
        data = str(r.headers)
        data = data.replace("\"","\<><>")
        data = data.replace("\'","\"")
        data = data.replace("\<><>","\'")
        data = json.loads(data)
        last_modified = data['Last-Modified']
        last_modified = parser.parse(last_modified)
        last_modified = last_modified.replace(tzinfo=None)
        last_modified = str(last_modified)
        
        return last_modified

    def framasFZ(self):
        site = sites_name+'/'+folder_budget().framasFZ()+'/'+file_name_budget().framasFZ()
        r = conn.get(site, headers=headers)
        f = io.BytesIO(r.content)
        data = str(r.headers)
        data = data.replace("\"","\<><>")
        data = data.replace("\'","\"")
        data = data.replace("\<><>","\'")
        data = json.loads(data)
        last_modified = data['Last-Modified']
        last_modified = parser.parse(last_modified)
        last_modified = last_modified.replace(tzinfo=None)
        last_modified = str(last_modified)
        
        return last_modified

    def framasHN(self):
        site = sites_name+'/'+folder_budget().framasHN()+'/'+file_name_budget().framasHN()
        r = conn.get(site, headers=headers)
        f = io.BytesIO(r.content)
        data = str(r.headers)
        data = data.replace("\"","\<><>")
        data = data.replace("\'","\"")
        data = data.replace("\<><>","\'")
        data = json.loads(data)
        last_modified = data['Last-Modified']
        last_modified = parser.parse(last_modified)
        last_modified = last_modified.replace(tzinfo=None)
        last_modified = str(last_modified)
        
        return last_modified

    def framasID(self):
        site = sites_name+'/'+folder_budget().framasID()+'/'+file_name_budget().framasID()
        r = conn.get(site, headers=headers)
        f = io.BytesIO(r.content)
        data = str(r.headers)
        data = data.replace("\"","\<><>")
        data = data.replace("\'","\"")
        data = data.replace("\<><>","\'")
        data = json.loads(data)
        last_modified = data['Last-Modified']
        last_modified = parser.parse(last_modified)
        last_modified = last_modified.replace(tzinfo=None)
        last_modified = str(last_modified)
        
        return last_modified

    def framasKR(self):
        site = sites_name+'/'+folder_budget().framasKR()+'/'+file_name_budget().framasKR()
        r = conn.get(site, headers=headers)
        f = io.BytesIO(r.content)
        data = str(r.headers)
        data = data.replace("\"","\<><>")
        data = data.replace("\'","\"")
        data = data.replace("\<><>","\'")
        data = json.loads(data)
        last_modified = data['Last-Modified']
        last_modified = parser.parse(last_modified)
        last_modified = last_modified.replace(tzinfo=None)
        last_modified = str(last_modified)
        
        return last_modified

    def framasKV(self):
        site = sites_name+'/'+folder_budget().framasKV()+'/'+file_name_budget().framasKV()
        r = conn.get(site, headers=headers)
        f = io.BytesIO(r.content)
        data = str(r.headers)
        data = data.replace("\"","\<><>")
        data = data.replace("\'","\"")
        data = data.replace("\<><>","\'")
        data = json.loads(data)
        last_modified = data['Last-Modified']
        last_modified = parser.parse(last_modified)
        last_modified = last_modified.replace(tzinfo=None)
        last_modified = str(last_modified)
        
        return last_modified

    def framasVN(self):
        site = sites_name+'/'+folder_budget().framasVN()+'/'+file_name_budget().framasVN()
        r = conn.get(site, headers=headers)
        f = io.BytesIO(r.content)
        data = str(r.headers)
        data = data.replace("\"","\<><>")
        data = data.replace("\'","\"")
        data = data.replace("\<><>","\'")
        data = json.loads(data)
        last_modified = data['Last-Modified']
        last_modified = parser.parse(last_modified)
        last_modified = last_modified.replace(tzinfo=None)
        last_modified = str(last_modified)
        
        return last_modified

class previous_modified_budget():
    def __init__(self):
        pass

    def framasDE(self):
        cursor.execute('''  EXEC sp_get_last_modified_file 11, 'LAST_MODIFIED_TIME_BUDGET'
                        ''')
        results = cursor.fetchone()
        for row in results:
            time = row

        return time

    def framasFZ(self):
        cursor.execute('''  EXEC sp_get_last_modified_file 55, 'LAST_MODIFIED_TIME_BUDGET'
                        ''')
        results = cursor.fetchone()
        for row in results:
            time = row

        return time

    def framasHN(self):
        cursor.execute('''  EXEC sp_get_last_modified_file 66, 'LAST_MODIFIED_TIME_BUDGET'
                        ''')
        results = cursor.fetchone()
        for row in results:
            time = row

        return time

    def framasID(self):
        cursor.execute('''  EXEC sp_get_last_modified_file 31, 'LAST_MODIFIED_TIME_BUDGET'
                        ''')
        results = cursor.fetchone()
        for row in results:
            time = row

        return time

    def framasKR(self):
        cursor.execute('''  EXEC sp_get_last_modified_file 21, 'LAST_MODIFIED_TIME_BUDGET'
                        ''')
        results = cursor.fetchone()
        for row in results:
            time = row

        return time

    def framasKV(self):
        cursor.execute('''  EXEC sp_get_last_modified_file 64, 'LAST_MODIFIED_TIME_BUDGET'
                        ''')
        results = cursor.fetchone()
        for row in results:
            time = row

        return time

    def framasVN(self):
        cursor.execute('''  EXEC sp_get_last_modified_file 61, 'LAST_MODIFIED_TIME_BUDGET'
                        ''')
        results = cursor.fetchone()
        for row in results:
            time = row

        return time

class create_new_modify_time_budget():
    def __init__(self):
        pass

    def framasDE(self):

        cursor.execute('''  EXEC sp_update_last_modified_file 11,'LAST_MODIFIED_TIME_BUDGET',%s
                        ''',(last_modified_budget().framasDE()))
        conn_sql.commit()

        return

    def framasFZ(self):

        cursor.execute('''  EXEC sp_update_last_modified_file 55,'LAST_MODIFIED_TIME_BUDGET',%s
                        ''',(last_modified_budget().framasFZ()))
        conn_sql.commit()

        return

    def framasHN(self):

        cursor.execute('''  EXEC sp_update_last_modified_file 66,'LAST_MODIFIED_TIME_BUDGET',%s
                        ''',(last_modified_budget().framasHN()))
        conn_sql.commit()

        return

    def framasID(self):
        
        cursor.execute('''  EXEC sp_update_last_modified_file 31,'LAST_MODIFIED_TIME_BUDGET',%s
                        ''',(last_modified_budget().framasID()))
        conn_sql.commit()

        return

    def framasKR(self):
        
        cursor.execute('''  EXEC sp_update_last_modified_file 21,'LAST_MODIFIED_TIME_BUDGET',%s
                        ''',(last_modified_budget().framasKR()))
        conn_sql.commit()

        return

    def framasKV(self):
        
        cursor.execute('''  EXEC sp_update_last_modified_file 64,'LAST_MODIFIED_TIME_BUDGET',%s
                        ''',(last_modified_budget().framasKV()))
        conn_sql.commit()

        return

    def framasVN(self):
        
        cursor.execute('''  EXEC sp_update_last_modified_file 61,'LAST_MODIFIED_TIME_BUDGET',%s
                        ''',(last_modified_budget().framasVN()))
        conn_sql.commit()

        return


conn = connect_with_sharepy().sharepoint()
headers = {'accept': 'application/atom+json'}

conn_sql = connect().sql_server()
cursor = conn_sql.cursor()