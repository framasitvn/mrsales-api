import os
import sys
sys.path.append(os.path.realpath('.'))
import pandas as pd
from pyxlsb import convert_date
from datetime import date, datetime
from read_excel_file.read_file import read_file_budget
from info.excel_file import star_row, end_row, budget_start_month, budget_end_month
from info.excel_file import facID_framasDE, facID_framasFZ, facID_framasHN, facID_framasID, facID_framasKR, facID_framasKV, facID_framasVN
from info.excel_file import budget_scenario_id


class current_month():
    def month(self):
        month = datetime.today().replace(day=1, hour=0, minute=0, second=0, microsecond=0)   # current_month = today with days = 1
        return month

class create_data_frame():
    def __init__(self):
        pass

    def framasDE(self):
        df = read_file_budget().framasDE()
        df = df.loc[star_row:end_row,:]
        return df

    def framasFZ(self):
        df = read_file_budget().framasFZ()
        df = df.loc[star_row:end_row,:]
        return df

    def framasHN(self):
        df = read_file_budget().framasHN()
        df = df.loc[star_row:end_row,:]
        return df

    def framasID(self):
        df = read_file_budget().framasID()
        df = df.loc[star_row:end_row,:]
        return df

    def framasKR(self):
        df = read_file_budget().framasKR()
        df = df.loc[star_row:end_row,:]
        return df

    def framasKV(self):
        df = read_file_budget().framasKV()
        df = df.loc[star_row:end_row,:]
        return df

    def framasVN(self):
        df = read_file_budget().framasVN()
        df = df.loc[star_row:end_row,:]
        return df

class info_columns():
    def __init__(self):
        pass

    def framasDE(self):
        df = create_data_frame().framasDE()

        df.columns = df.iloc[2]                                 # Set title with row id = 2
        df = df.drop([df.index[0],df.index[1],df.index[2]])     # Drop 3 row empty

        df.index.name = 'Index Number'                          # Add columns 'Index Number'
        df.reset_index(inplace=True)                            # Reset index

        factory = df[['Factory']]
        project_number = df[['Product No.']]                    # Project Number like Product No in excel file
        descripton = df.iloc[:, 19]                             # Descripton
        index_excel_file = (df[['Index Number']] + 2)           # Index Number

        df = pd.concat([factory, index_excel_file, project_number, descripton], axis=1)     # Merge dataframe

        return df

    def framasFZ(self):
        df = create_data_frame().framasFZ()

        df.columns = df.iloc[2]                                 # Set title with row id = 2
        df = df.drop([df.index[0],df.index[1],df.index[2]])     # Drop 3 row empty

        df.index.name = 'Index Number'                          # Add columns 'Index Number'
        df.reset_index(inplace=True)                            # Reset index

        factory = df[['Factory']]
        project_number = df[['Product No.']]                    # Project Number like Product No in excel file
        descripton = df.iloc[:, 19]                             # Descripton
        index_excel_file = (df[['Index Number']] + 2)           # Index Number

        df = pd.concat([factory, index_excel_file, project_number, descripton], axis=1)     # Merge dataframe

        return df

    def framasHN(self):
        df = create_data_frame().framasHN()
        
        df.columns = df.iloc[2]                                 # Set title with row id = 2
        df = df.drop([df.index[0],df.index[1],df.index[2]])     # Drop 3 row empty

        df.index.name = 'Index Number'                          # Add columns 'Index Number'
        df.reset_index(inplace=True)                            # Reset index

        factory = df[['Factory']]
        project_number = df[['Product No.']]                    # Project Number like Product No in excel file
        descripton = df.iloc[:, 19]                             # Descripton
        index_excel_file = (df[['Index Number']] + 2)           # Index Number

        df = pd.concat([factory, index_excel_file, project_number, descripton], axis=1)     # Merge dataframe

        return df

    def framasID(self):
        df = create_data_frame().framasID()
        
        df.columns = df.iloc[2]                                 # Set title with row id = 2
        df = df.drop([df.index[0],df.index[1],df.index[2]])     # Drop 3 row empty

        df.index.name = 'Index Number'                          # Add columns 'Index Number'
        df.reset_index(inplace=True)                            # Reset index

        factory = df[['Factory']]
        project_number = df[['Product No.']]                    # Project Number like Product No in excel file
        descripton = df.iloc[:, 19]                             # Descripton
        index_excel_file = (df[['Index Number']] + 2)           # Index Number

        df = pd.concat([factory, index_excel_file, project_number, descripton], axis=1)     # Merge dataframe

        return df

    def framasKR(self):
        df = create_data_frame().framasKR()
        
        df.columns = df.iloc[2]                                 # Set title with row id = 2
        df = df.drop([df.index[0],df.index[1],df.index[2]])     # Drop 3 row empty

        df.index.name = 'Index Number'                          # Add columns 'Index Number'
        df.reset_index(inplace=True)                            # Reset index

        factory = df[['Factory']]
        project_number = df[['Product No.']]                    # Project Number like Product No in excel file
        descripton = df.iloc[:, 19]                             # Descripton
        index_excel_file = (df[['Index Number']] + 2)           # Index Number

        df = pd.concat([factory, index_excel_file, project_number, descripton], axis=1)     # Merge dataframe

        return df

    def framasKV(self):
        df = create_data_frame().framasKV()
        
        df.columns = df.iloc[2]                                 # Set title with row id = 2
        df = df.drop([df.index[0],df.index[1],df.index[2]])     # Drop 3 row empty

        df.index.name = 'Index Number'                          # Add columns 'Index Number'
        df.reset_index(inplace=True)                            # Reset index

        factory = df[['Factory']]
        project_number = df[['Product No.']]                    # Project Number like Product No in excel file
        descripton = df.iloc[:, 19]                             # Descripton
        index_excel_file = (df[['Index Number']] + 2)           # Index Number

        df = pd.concat([factory, index_excel_file, project_number, descripton], axis=1)     # Merge dataframe

        return df

    def framasVN(self):
        df = create_data_frame().framasVN()

        df.columns = df.iloc[2]                                 # Set title with row id = 2
        df = df.drop([df.index[0],df.index[1],df.index[2]])     # Drop 3 row empty

        df.index.name = 'Index Number'                          # Add columns 'Index Number'
        df.reset_index(inplace=True)                            # Reset index

        factory = df[['Factory']]
        project_number = df[['Product No.']]                    # Project Number like Product No in excel file
        descripton = df.iloc[:, 19]                             # Descripton
        index_excel_file = (df[['Index Number']] + 2)           # Index Number

        df = pd.concat([factory, index_excel_file, project_number, descripton], axis=1)     # Merge dataframe

        return df

class date_columns():
    def __init__(self):
        pass

    def framasDE(self):
        df = create_data_frame().framasDE()
        
        df = df.filter(like="Real (M)")

        df.columns = df.iloc[2]             # --- Take 2nd row as title
        df = df.loc[:, :2009]               # --- Taken from the first column to the '2009' column
        df = df.drop(2009, axis=1)          # --- Drop column '2009'

        df = df.drop([df.index[0],df.index[1],df.index[2]])   # --- Drop 3 empty row under title
        df = df.reset_index(drop=True)      # --- Reset index

        columns = []
        for column in df.columns:
            date = convert_date(column)         # --- Convert each column to date format            
            columns.append(date)

        df.columns = columns
            
        df = df.loc[:, budget_start_month:budget_end_month]

        return df
        
    def framasFZ(self):
        df = create_data_frame().framasFZ()
        
        df = df.filter(like="Real (M)")

        df.columns = df.iloc[2]             # --- Take 2nd row as title
        df = df.loc[:, :2009]               # --- Taken from the first column to the '2009' column
        df = df.drop(2009, axis=1)          # --- Drop column '2009'

        df = df.drop([df.index[0],df.index[1],df.index[2]])   # --- Drop 3 empty row under title
        df = df.reset_index(drop=True)      # --- Reset index

        columns = []
        for column in df.columns:
            date = convert_date(column)         # --- Convert each column to date format            
            columns.append(date)

        df.columns = columns  

        df = df.loc[:, budget_start_month:budget_end_month]

        return df

    def framasHN(self):
        df = create_data_frame().framasHN()
        
        df = df.filter(like="Real (M)")

        df.columns = df.iloc[2]             # --- Take 2nd row as title
        df = df.loc[:, :2009]               # --- Taken from the first column to the '2009' column
        df = df.drop(2009, axis=1)          # --- Drop column '2009'

        df = df.drop([df.index[0],df.index[1],df.index[2]])   # --- Drop 3 empty row under title
        df = df.reset_index(drop=True)      # --- Reset index

        columns = []
        for column in df.columns:
            date = convert_date(column)         # --- Convert each column to date format            
            columns.append(date)

        df.columns = columns   

        df = df.loc[:, budget_start_month:budget_end_month]

        return df

    def framasID(self):
        df = create_data_frame().framasID()
        
        df = df.filter(like="Real (M)")

        df.columns = df.iloc[2]             # --- Take 2nd row as title
        df = df.loc[:, :2009]               # --- Taken from the first column to the '2009' column
        df = df.drop(2009, axis=1)          # --- Drop column '2009'

        df = df.drop([df.index[0],df.index[1],df.index[2]])   # --- Drop 3 empty row under title
        df = df.reset_index(drop=True)      # --- Reset index

        columns = []
        for column in df.columns:
            date = convert_date(column)         # --- Convert each column to date format            
            columns.append(date)

        df.columns = columns     

        df = df.loc[:, budget_start_month:budget_end_month]

        return df

    def framasKR(self):
        df = create_data_frame().framasKR()
        
        df = df.filter(like="Real (M)")

        df.columns = df.iloc[2]             # --- Take 2nd row as title
        df = df.loc[:, :2009]               # --- Taken from the first column to the '2009' column
        df = df.drop(2009, axis=1)          # --- Drop column '2009'

        df = df.drop([df.index[0],df.index[1],df.index[2]])   # --- Drop 3 empty row under title
        df = df.reset_index(drop=True)      # --- Reset index

        columns = []
        for column in df.columns:
            date = convert_date(column)         # --- Convert each column to date format            
            columns.append(date)

        df.columns = columns       

        df = df.loc[:, budget_start_month:budget_end_month]

        return df

    def framasKV(self):
        df = create_data_frame().framasKV()
        
        df = df.filter(like="Real (M)")

        df.columns = df.iloc[2]             # --- Take 2nd row as title
        df = df.loc[:, :2009]               # --- Taken from the first column to the '2009' column
        df = df.drop(2009, axis=1)          # --- Drop column '2009'

        df = df.drop([df.index[0],df.index[1],df.index[2]])   # --- Drop 3 empty row under title
        df = df.reset_index(drop=True)      # --- Reset index

        columns = []
        for column in df.columns:
            date = convert_date(column)         # --- Convert each column to date format            
            columns.append(date)

        df.columns = columns      

        df = df.loc[:, budget_start_month:budget_end_month]

        return df

    def framasVN(self):
        df = create_data_frame().framasVN()

        df = df.filter(like="Real (M)")

        df.columns = df.iloc[2]             # --- Take 2nd row as title
        df = df.loc[:, :2009]               # --- Taken from the first column to the '2009' column
        df = df.drop(2009, axis=1)          # --- Drop column '2009'

        df = df.drop([df.index[0],df.index[1],df.index[2]])   # --- Drop 3 empty row under title
        df = df.reset_index(drop=True)      # --- Reset index

        columns = []
        for column in df.columns:
            date = convert_date(column)         # --- Convert each column to date format            
            columns.append(date)

        df.columns = columns   

        df = df.loc[:, budget_start_month:budget_end_month]
             
        return df

class concat_date_frame():
    def __init__(self):
        pass

    def framasDE(self):
        a = info_columns().framasDE()
        b = date_columns().framasDE()

        df = pd.concat([a, b], axis=1)

        return df

    def framasFZ(self):
        a = info_columns().framasFZ()
        b = date_columns().framasFZ()

        df = pd.concat([a, b], axis=1)

        return df

    def framasHN(self):
        a = info_columns().framasHN()
        b = date_columns().framasHN()

        df = pd.concat([a, b], axis=1)

        return df

    def framasID(self):
        a = info_columns().framasID()
        b = date_columns().framasID()

        df = pd.concat([a, b], axis=1)

        return df

    def framasKR(self):
        a = info_columns().framasKR()
        b = date_columns().framasKR()

        df = pd.concat([a, b], axis=1)

        return df

    def framasKV(self):
        a = info_columns().framasKV()
        b = date_columns().framasKV()

        df = pd.concat([a, b], axis=1)

        return df

    def framasVN(self):
        a = info_columns().framasVN()
        b = date_columns().framasVN()

        df = pd.concat([a, b], axis=1)

        return df

class melt_data_frame():
    def __init__(self):
        pass

    def framasDE(self):
        df = concat_date_frame().framasDE()
        
        df = pd.melt(df, id_vars=['Index Number', 'Factory', 'Product No.', 'Description'], var_name='Date', value_name='Values')
        
        df = df.query("Values != 0")
        df = df[~df['Values'].isnull()]
        df.columns = ['MRS_INDEX', 'MRS_FACTORY_ID', 'MRS_PROJECT_NO', 'MRS_DATA_TYPE', 'MRS_SALE_DATE', 'MRS_SALE_VALUE']      # Rename same column name in database
        df[['MRS_FACTORY_ID']] = facID_framasDE
        
        return df

    def framasFZ(self):
        df = concat_date_frame().framasFZ()
        
        df = pd.melt(df, id_vars=['Index Number', 'Factory', 'Product No.', 'Description'], var_name='Date', value_name='Values')
        
        df = df.query("Values != 0")
        df = df[~df['Values'].isnull()]
        df.columns = ['MRS_INDEX', 'MRS_FACTORY_ID', 'MRS_PROJECT_NO', 'MRS_DATA_TYPE', 'MRS_SALE_DATE', 'MRS_SALE_VALUE']      # Rename same column name in database
        df[['MRS_FACTORY_ID']] = facID_framasFZ
        
        return df

    def framasHN(self):
        df = concat_date_frame().framasHN()
        
        df = pd.melt(df, id_vars=['Index Number', 'Factory', 'Product No.', 'Description'], var_name='Date', value_name='Values')
        
        df = df.query("Values != 0")
        df = df[~df['Values'].isnull()]
        df.columns = ['MRS_INDEX', 'MRS_FACTORY_ID', 'MRS_PROJECT_NO', 'MRS_DATA_TYPE', 'MRS_SALE_DATE', 'MRS_SALE_VALUE']      # Rename same column name in database
        df[['MRS_FACTORY_ID']] = facID_framasHN
        
        return df

    def framasID(self):
        df = concat_date_frame().framasID()
        
        df = pd.melt(df, id_vars=['Index Number', 'Factory', 'Product No.', 'Description'], var_name='Date', value_name='Values')
        
        df = df.query("Values != 0")
        df = df[~df['Values'].isnull()]
        df.columns = ['MRS_INDEX', 'MRS_FACTORY_ID', 'MRS_PROJECT_NO', 'MRS_DATA_TYPE', 'MRS_SALE_DATE', 'MRS_SALE_VALUE']      # Rename same column name in database
        df[['MRS_FACTORY_ID']] = facID_framasID
        
        return df

    def framasKR(self):
        df = concat_date_frame().framasKR()
        
        df = pd.melt(df, id_vars=['Index Number', 'Factory', 'Product No.', 'Description'], var_name='Date', value_name='Values')
        
        df = df.query("Values != 0")
        df = df[~df['Values'].isnull()]
        df.columns = ['MRS_INDEX', 'MRS_FACTORY_ID', 'MRS_PROJECT_NO', 'MRS_DATA_TYPE', 'MRS_SALE_DATE', 'MRS_SALE_VALUE']      # Rename same column name in database
        df[['MRS_FACTORY_ID']] = facID_framasKR
        
        return df

    def framasKV(self):
        df = concat_date_frame().framasKV()
        
        df = pd.melt(df, id_vars=['Index Number', 'Factory', 'Product No.', 'Description'], var_name='Date', value_name='Values')
        
        df = df.query("Values != 0")
        df = df[~df['Values'].isnull()]
        df.columns = ['MRS_INDEX', 'MRS_FACTORY_ID', 'MRS_PROJECT_NO', 'MRS_DATA_TYPE', 'MRS_SALE_DATE', 'MRS_SALE_VALUE']      # Rename same column name in database
        df[['MRS_FACTORY_ID']] = facID_framasKV
        
        return df

    def framasVN(self):
        df = concat_date_frame().framasVN()
        
        df = pd.melt(df, id_vars=['Index Number', 'Factory', 'Product No.', 'Description'], var_name='Date', value_name='Values')
        
        df = df.query("Values != 0")
        df = df[~df['Values'].isnull()]
        df.columns = ['MRS_INDEX', 'MRS_FACTORY_ID', 'MRS_PROJECT_NO', 'MRS_DATA_TYPE', 'MRS_SALE_DATE', 'MRS_SALE_VALUE']      # Rename same column name in database
        df[['MRS_FACTORY_ID']] = facID_framasVN
        
        return df

class budget_dataframe():
    def __init__(self):
        pass

    def framasDE(self):
        df = melt_data_frame().framasDE()
        
        df['MRS_ROLLING_DATE'] = df['MRS_SALE_DATE']        # Add new column 'MRS_ROLLING_DATE' with value = column 'MRS_SALE_DATE'
        df['MRS_SCENARIO_ID'] = budget_scenario_id                           # Add new column 'MRS_SCENARIO_ID' with default values = 1
        df = df[['MRS_FACTORY_ID', 'MRS_INDEX', 'MRS_PROJECT_NO', 'MRS_SALE_DATE', 'MRS_DATA_TYPE', 'MRS_SALE_VALUE', 'MRS_ROLLING_DATE', 'MRS_SCENARIO_ID']]       # Rearrange the order of columns
        
        month = current_month().month()

        df.loc[(df['MRS_ROLLING_DATE'] >= month), 'MRS_ROLLING_DATE'] = month
        df['MRS_PROJECT_NO'] = df['MRS_PROJECT_NO'].astype(str)

        return df

    def framasFZ(self):
        df = melt_data_frame().framasFZ()
        
        df['MRS_ROLLING_DATE'] = df['MRS_SALE_DATE']        # Add new column 'MRS_ROLLING_DATE' with value = column 'MRS_SALE_DATE'
        df['MRS_SCENARIO_ID'] = budget_scenario_id                           # Add new column 'MRS_SCENARIO_ID' with default values = 1
        df = df[['MRS_FACTORY_ID', 'MRS_INDEX', 'MRS_PROJECT_NO', 'MRS_SALE_DATE', 'MRS_DATA_TYPE', 'MRS_SALE_VALUE', 'MRS_ROLLING_DATE', 'MRS_SCENARIO_ID']]       # Rearrange the order of columns
        
        month = current_month().month()

        df.loc[(df['MRS_ROLLING_DATE'] >= month), 'MRS_ROLLING_DATE'] = month
        df['MRS_PROJECT_NO'] = df['MRS_PROJECT_NO'].astype(str)

        return df

    def framasHN(self):
        df = melt_data_frame().framasHN()
        
        df['MRS_ROLLING_DATE'] = df['MRS_SALE_DATE']        # Add new column 'MRS_ROLLING_DATE' with value = column 'MRS_SALE_DATE'
        df['MRS_SCENARIO_ID'] = budget_scenario_id                           # Add new column 'MRS_SCENARIO_ID' with default values = 1
        df = df[['MRS_FACTORY_ID', 'MRS_INDEX', 'MRS_PROJECT_NO', 'MRS_SALE_DATE', 'MRS_DATA_TYPE', 'MRS_SALE_VALUE', 'MRS_ROLLING_DATE', 'MRS_SCENARIO_ID']]       # Rearrange the order of columns
        
        month = current_month().month()

        df.loc[(df['MRS_ROLLING_DATE'] >= month), 'MRS_ROLLING_DATE'] = month
        df['MRS_PROJECT_NO'] = df['MRS_PROJECT_NO'].astype(str)

        return df

    def framasID(self):
        df = melt_data_frame().framasID()
        
        df['MRS_ROLLING_DATE'] = df['MRS_SALE_DATE']        # Add new column 'MRS_ROLLING_DATE' with value = column 'MRS_SALE_DATE'
        df['MRS_SCENARIO_ID'] = budget_scenario_id                           # Add new column 'MRS_SCENARIO_ID' with default values = 1
        df = df[['MRS_FACTORY_ID', 'MRS_INDEX', 'MRS_PROJECT_NO', 'MRS_SALE_DATE', 'MRS_DATA_TYPE', 'MRS_SALE_VALUE', 'MRS_ROLLING_DATE', 'MRS_SCENARIO_ID']]       # Rearrange the order of columns
        
        month = current_month().month()

        df.loc[(df['MRS_ROLLING_DATE'] >= month), 'MRS_ROLLING_DATE'] = month
        df['MRS_PROJECT_NO'] = df['MRS_PROJECT_NO'].astype(str)

        return df

    def framasKR(self):
        df = melt_data_frame().framasKR()
        
        df['MRS_ROLLING_DATE'] = df['MRS_SALE_DATE']        # Add new column 'MRS_ROLLING_DATE' with value = column 'MRS_SALE_DATE'
        df['MRS_SCENARIO_ID'] = budget_scenario_id                           # Add new column 'MRS_SCENARIO_ID' with default values = 1
        df = df[['MRS_FACTORY_ID', 'MRS_INDEX', 'MRS_PROJECT_NO', 'MRS_SALE_DATE', 'MRS_DATA_TYPE', 'MRS_SALE_VALUE', 'MRS_ROLLING_DATE', 'MRS_SCENARIO_ID']]       # Rearrange the order of columns
        
        month = current_month().month()

        df.loc[(df['MRS_ROLLING_DATE'] >= month), 'MRS_ROLLING_DATE'] = month
        df['MRS_PROJECT_NO'] = df['MRS_PROJECT_NO'].astype(str)

        return df

    def framasKV(self):
        df = melt_data_frame().framasKV()
        
        df['MRS_ROLLING_DATE'] = df['MRS_SALE_DATE']        # Add new column 'MRS_ROLLING_DATE' with value = column 'MRS_SALE_DATE'
        df['MRS_SCENARIO_ID'] = budget_scenario_id                           # Add new column 'MRS_SCENARIO_ID' with default values = 1
        df = df[['MRS_FACTORY_ID', 'MRS_INDEX', 'MRS_PROJECT_NO', 'MRS_SALE_DATE', 'MRS_DATA_TYPE', 'MRS_SALE_VALUE', 'MRS_ROLLING_DATE', 'MRS_SCENARIO_ID']]       # Rearrange the order of columns
        
        month = current_month().month()

        df.loc[(df['MRS_ROLLING_DATE'] >= month), 'MRS_ROLLING_DATE'] = month
        df['MRS_PROJECT_NO'] = df['MRS_PROJECT_NO'].astype(str)

        return df

    def framasVN(self):
        df = melt_data_frame().framasVN()
        
        df['MRS_ROLLING_DATE'] = df['MRS_SALE_DATE']        # Add new column 'MRS_ROLLING_DATE' with value = column 'MRS_SALE_DATE'
        df['MRS_SCENARIO_ID'] = budget_scenario_id                           # Add new column 'MRS_SCENARIO_ID' with default values = 1
        df = df[['MRS_FACTORY_ID', 'MRS_INDEX', 'MRS_PROJECT_NO', 'MRS_SALE_DATE', 'MRS_DATA_TYPE', 'MRS_SALE_VALUE', 'MRS_ROLLING_DATE', 'MRS_SCENARIO_ID']]       # Rearrange the order of columns
        
        month = current_month().month()

        df.loc[(df['MRS_ROLLING_DATE'] >= month), 'MRS_ROLLING_DATE'] = month
        df['MRS_PROJECT_NO'] = df['MRS_PROJECT_NO'].astype(str)

        return df
