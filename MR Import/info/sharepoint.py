import os
import sys
sys.path.append(os.path.realpath('.'))
from datetime import date, datetime
from connect.database import connect

#-------------- Information login --------------#
server = 'https://framas365.sharepoint.com'
user = 'lieu.dong@framas.com'
password = 'li34Don#'

#-------------- Url sharepoint site --------------#
sites_name = 'https://framas365.sharepoint.com/sites/MRframasgroup'

#-------------- Name of the folder default in Sharepoint --------------#
folder_url = 'Shared Documents/General'


class today():

    def __init__(self):
        pass

    def today(self):
        today = datetime.today()
        month = today.month
        if month == 1:
            month = 12
            year = today.year -1
        else:
            month = month - 1
            year = today.year

        month = '{:02d}'.format(month)
        year = str(year)

        time_folder = year+'/'+month

        return time_folder

    def year(self):
        today = datetime.today()
        year = str(today.year)

        return year

class folder():

    def __init__(self):
        pass

    def framasDE(self):

        cursor.execute('''  EXEC sp_get_file_folder 'MAS_FACTORY', 11
                        ''')
        results = cursor.fetchone()
        for row in results:
            factory_des = row

        folder_name = folder_url+'/'+factory_des+'/'+time_folder

        return folder_name

    def framasFZ(self):
        cursor.execute('''  EXEC sp_get_file_folder 'MAS_FACTORY', 55
                        ''')
        results = cursor.fetchone()
        for row in results:
            factory_des = row

        folder_name = folder_url+'/'+factory_des+'/'+time_folder

        return folder_name

    def framasHN(self):
        cursor.execute('''  EXEC sp_get_file_folder 'MAS_FACTORY', 66
                        ''')
        results = cursor.fetchone()
        for row in results:
            factory_des = row

        folder_name = folder_url+'/'+factory_des+'/'+time_folder

        return folder_name

    def framasID(self):
        cursor.execute('''  EXEC sp_get_file_folder 'MAS_FACTORY', 31
                        ''')
        results = cursor.fetchone()
        for row in results:
            factory_des = row

        folder_name = folder_url+'/'+factory_des+'/'+time_folder

        return folder_name

    def framasKR(self):
        cursor.execute('''  EXEC sp_get_file_folder 'MAS_FACTORY', 21
                        ''')
        results = cursor.fetchone()
        for row in results:
            factory_des = row

        folder_name = folder_url+'/'+factory_des+'/'+time_folder

        return folder_name

    def framasKV(self):
        cursor.execute('''  EXEC sp_get_file_folder 'MAS_FACTORY', 64
                        ''')
        results = cursor.fetchone()
        for row in results:
            factory_des = row

        folder_name = folder_url+'/'+factory_des+'/'+time_folder

        return folder_name

    def framasVN(self):
        cursor.execute('''  EXEC sp_get_file_folder 'MAS_FACTORY', 61
                        ''')
        results = cursor.fetchone()
        for row in results:
            factory_des = row

        folder_name = folder_url+'/'+factory_des+'/'+time_folder

        return folder_name

class folder_budget():
    def __init__(self):
        pass

    def framasDE(self):
        cursor.execute('''  EXEC sp_get_file_folder 'MAS_FACTORY', 11
                        ''')
        results1 = cursor.fetchone()
        for row in results1:
            factory_des_1 = row

        cursor.execute('''  EXEC sp_get_file_folder 'MR_SALES_FILE_BUDGET', 11
                        ''')
        results2 = cursor.fetchone()
        for row in results2:
            factory_des_2 = row

        folder_name = folder_url+'/'+factory_des_1+'/'+current_year+'/'+factory_des_2

        return folder_name

    def framasFZ(self):
        cursor.execute('''  EXEC sp_get_file_folder 'MAS_FACTORY', 55
                        ''')
        results1 = cursor.fetchone()
        for row in results1:
            factory_des_1 = row

        cursor.execute('''  EXEC sp_get_file_folder 'MR_SALES_FILE_BUDGET', 55
                        ''')
        results2 = cursor.fetchone()
        for row in results2:
            factory_des_2 = row

        folder_name = folder_url+'/'+factory_des_1+'/'+current_year+'/'+factory_des_2

        return folder_name

    def framasHN(self):
        cursor.execute('''  EXEC sp_get_file_folder 'MAS_FACTORY', 66
                        ''')
        results1 = cursor.fetchone()
        for row in results1:
            factory_des_1 = row

        cursor.execute('''  EXEC sp_get_file_folder 'MR_SALES_FILE_BUDGET', 66
                        ''')
        results2 = cursor.fetchone()
        for row in results2:
            factory_des_2 = row

        folder_name = folder_url+'/'+factory_des_1+'/'+current_year+'/'+factory_des_2

        return folder_name

    def framasID(self):
        cursor.execute('''  EXEC sp_get_file_folder 'MAS_FACTORY', 31
                        ''')
        results1 = cursor.fetchone()
        for row in results1:
            factory_des_1 = row

        cursor.execute('''  EXEC sp_get_file_folder 'MR_SALES_FILE_BUDGET', 31
                        ''')
        results2 = cursor.fetchone()
        for row in results2:
            factory_des_2 = row

        folder_name = folder_url+'/'+factory_des_1+'/'+current_year+'/'+factory_des_2

        return folder_name

    def framasKR(self):
        cursor.execute('''  EXEC sp_get_file_folder 'MAS_FACTORY', 21
                        ''')
        results1 = cursor.fetchone()
        for row in results1:
            factory_des_1 = row

        cursor.execute('''  EXEC sp_get_file_folder 'MR_SALES_FILE_BUDGET', 21
                        ''')
        results2 = cursor.fetchone()
        for row in results2:
            factory_des_2 = row

        folder_name = folder_url+'/'+factory_des_1+'/'+current_year+'/'+factory_des_2

        return folder_name

    def framasKV(self):
        cursor.execute('''  EXEC sp_get_file_folder 'MAS_FACTORY', 64
                        ''')
        results1 = cursor.fetchone()
        for row in results1:
            factory_des_1 = row

        cursor.execute('''  EXEC sp_get_file_folder 'MR_SALES_FILE_BUDGET', 64
                        ''')
        results2 = cursor.fetchone()
        for row in results2:
            factory_des_2 = row

        folder_name = folder_url+'/'+factory_des_1+'/'+current_year+'/'+factory_des_2

        return folder_name

    def framasVN(self):
        cursor.execute('''  EXEC sp_get_file_folder 'MAS_FACTORY', 61
                        ''')
        results1 = cursor.fetchone()
        for row in results1:
            factory_des_1 = row

        cursor.execute('''  EXEC sp_get_file_folder 'MR_SALES_FILE_BUDGET', 61
                        ''')
        results2 = cursor.fetchone()
        for row in results2:
            factory_des_2 = row

        folder_name = folder_url+'/'+factory_des_1+'/'+current_year+'/'+factory_des_2

        return folder_name
      
class file_name():
    
    def __init__(self):
        pass

    def framasDE(self):
        cursor.execute('''  EXEC sp_get_file_name 'MR_SALES_FILE', 11
                        ''')
        results = cursor.fetchone()
        for row in results:
            name = row

        return name

    def framasFZ(self):
        cursor.execute('''  EXEC sp_get_file_name 'MR_SALES_FILE', 55
                        ''')
        results = cursor.fetchone()
        for row in results:
            name = row

        return name

    def framasHN(self):
        cursor.execute('''  EXEC sp_get_file_name 'MR_SALES_FILE', 66
                        ''')
        results = cursor.fetchone()
        for row in results:
            name = row

        return name

    def framasID(self):
        cursor.execute('''  EXEC sp_get_file_name 'MR_SALES_FILE', 31
                        ''')
        results = cursor.fetchone()
        for row in results:
            name = row

        return name

    def framasKR(self):
        cursor.execute('''  EXEC sp_get_file_name 'MR_SALES_FILE', 21
                        ''')
        results = cursor.fetchone()
        for row in results:
            name = row

        return name

    def framasKV(self):
        cursor.execute('''  EXEC sp_get_file_name 'MR_SALES_FILE', 64
                        ''')
        results = cursor.fetchone()
        for row in results:
            name = row

        return name

    def framasVN(self):
        cursor.execute('''  EXEC sp_get_file_name 'MR_SALES_FILE', 61
                        ''')
        results = cursor.fetchone()
        for row in results:
            name = row

        return name

class file_name_budget():
    
    def __init__(self):
        pass

    def framasDE(self):
        cursor.execute('''  EXEC sp_get_file_name 'MR_SALES_FILE_BUDGET', 11
                        ''')
        results = cursor.fetchone()
        for row in results:
            name = row

        return name

    def framasFZ(self):
        cursor.execute('''  EXEC sp_get_file_name 'MR_SALES_FILE_BUDGET', 55
                        ''')
        results = cursor.fetchone()
        for row in results:
            name = row

        return name

    def framasHN(self):
        cursor.execute('''  EXEC sp_get_file_name 'MR_SALES_FILE_BUDGET', 66
                        ''')
        results = cursor.fetchone()
        for row in results:
            name = row

        return name

    def framasID(self):
        cursor.execute('''  EXEC sp_get_file_name 'MR_SALES_FILE_BUDGET', 31
                        ''')
        results = cursor.fetchone()
        for row in results:
            name = row

        return name

    def framasKR(self):
        cursor.execute('''  EXEC sp_get_file_name 'MR_SALES_FILE_BUDGET', 21
                        ''')
        results = cursor.fetchone()
        for row in results:
            name = row

        return name

    def framasKV(self):
        cursor.execute('''  EXEC sp_get_file_name 'MR_SALES_FILE_BUDGET', 64
                        ''')
        results = cursor.fetchone()
        for row in results:
            name = row

        return name

    def framasVN(self):
        cursor.execute('''  EXEC sp_get_file_name 'MR_SALES_FILE_BUDGET', 61
                        ''')
        results = cursor.fetchone()
        for row in results:
            name = row

        return name


conn = connect().sql_server()
cursor = conn.cursor()

time_folder = today().today()
current_year = today().year()




