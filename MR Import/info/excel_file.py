from datetime import date, datetime

#-------------- Sheet name & engine --------------#
sheet_name = 'Sales'
engine = 'pyxlsb'
star_row = 0
end_row = 1998          # --- 0 => 1998 like 2000

#-------------- Factory ID --------------#
facID_framasDE = 11
facID_framasFZ = 55
facID_framasHN = 66
facID_framasID = 31
facID_framasKR = 21
facID_framasKV = 64
facID_framasVN = 61

#-------------- Scenario ID --------------#
actual_scenario_id = 1
budget_scenario_id = 2
forecast_scenario_id = 3

#-------------- Columns budget file --------------#
budget_start_month = datetime.today().replace(day=1, month=1, hour=0, minute=0, second=0, microsecond=0)
budget_end_month = datetime.today().replace(day=1, month=12, hour=0, minute=0, second=0, microsecond=0)
