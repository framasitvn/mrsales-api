import os
import sys
sys.path.append(os.path.realpath('.'))
import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning)
import pymssql
from info.database import server, port, user, password, database



class connect():
    def __init__(self):
        pass

    def sql_server(self):
        conn = pymssql.connect(
        server=server,
        port=port,
        user=user,
        password=password,
        database=database)

        return conn
