import os
import sys
sys.path.append(os.path.realpath('.'))
from shareplum import Office365
from shareplum import Site
from shareplum.site import Version
from info.sharepoint import server, user, password, sites_name


class connect():
    def __init__(self):
        pass

    def sharepoint(self):
        authcookie = Office365(share_point_site=server, username=user, password=password).GetCookies()
        site = Site(sites_name, version=Version.v2016, authcookie=authcookie)
        return site