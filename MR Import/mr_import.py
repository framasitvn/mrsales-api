import os
import sys
sys.path.append(os.path.realpath('.'))
from connect.database import connect
from last_modified.get_last_modified_time import last_modified_actual_forecast, previous_modified_actual_forecast, create_new_modify_time_actual_forecast, last_modified_budget, previous_modified_budget, create_new_modify_time_budget
from import_data.imported import actual_forecast, budget
import time

class actual_forecast_type():
    def __init__(self):
        pass

    def framasDE(self):
        print("\n____framasDE____")

        a = previous_modified_actual_forecast().framasDE()
        b = last_modified_actual_forecast().framasDE()
        print("Previous modified: ",a)
        print("Last modified:     ",b)

        if a != b:
            print("\nFound a newly uploaded file !")
            print("Please wait to import data from the new file into the database !\n")

            actual_forecast().framasDE()
            create_new_modify_time_actual_forecast().framasDE()
            print('framasDE data has been imported into database !\n')
        else:
            print('No new files have been uploaded !\n')

        return

    def framasFZ(self):
        print("\n____framasDE____")
        
        a = previous_modified_actual_forecast().framasFZ()
        b = last_modified_actual_forecast().framasFZ()
        print("Previous modified: ",a)
        print("Last modified:     ",b)

        if a != b:
            print("\nFound a newly uploaded file !")
            print("Please wait to import data from the new file into the database !\n")

            actual_forecast().framasFZ()
            create_new_modify_time_actual_forecast().framasFZ()
            print('framasFZ data has been imported into database !\n')
        else:
            print('No new files have been uploaded !\n')

        return
        
    def framasHN(self):
        print("\n____framasHN____")
        
        a = previous_modified_actual_forecast().framasHN()
        b = last_modified_actual_forecast().framasHN()
        print("Previous modified: ",a)
        print("Last modified:     ",b)

        if a != b:
            print("\nFound a newly uploaded file !")
            print("Please wait to import data from the new file into the database !\n")

            actual_forecast().framasHN()
            create_new_modify_time_actual_forecast().framasHN()
            print('framasHN data has been imported into database !\n')
        else:
            print('No new files have been uploaded !\n')

        return
        
    def framasID(self):
        print("\n____framasID____")
        
        a = previous_modified_actual_forecast().framasID()
        b = last_modified_actual_forecast().framasID()
        print("Previous modified: ",a)
        print("Last modified:     ",b)

        if a != b:
            print("\nFound a newly uploaded file !")
            print("Please wait to import data from the new file into the database !\n")

            actual_forecast().framasID()
            create_new_modify_time_actual_forecast().framasID()
            print('framasID data has been imported into database !\n')
        else:
            print('No new files have been uploaded !\n')

        return
        
    def framasKR(self):
        print("\n____framasKR____")

        a = previous_modified_actual_forecast().framasKR()
        b = last_modified_actual_forecast().framasKR()
        print("Previous modified: ",a)
        print("Last modified:     ",b)

        if a != b:
            print("\nFound a newly uploaded file !")
            print("Please wait to import data from the new file into the database !\n")

            actual_forecast().framasKR()
            create_new_modify_time_actual_forecast().framasKR()
            print('framasKR data has been imported into database !\n')
        else:
            print('No new files have been uploaded !\n')

        return
        
    def framasKV(self):
        print("\n____framasKV____")

        a = previous_modified_actual_forecast().framasKV()
        b = last_modified_actual_forecast().framasKV()
        print("Previous modified: ",a)
        print("Last modified:     ",b)

        if a != b:
            print("\nFound a newly uploaded file !")
            print("Please wait to import data from the new file into the database !\n")

            actual_forecast().framasKV()
            create_new_modify_time_actual_forecast().framasKV()
            print('framasKV data has been imported into database !\n')
        else:
            print('No new files have been uploaded !\n')

        return
        
    def framasVN(self):
        print("\n____framasVN____")

        a = previous_modified_actual_forecast().framasVN()
        b = last_modified_actual_forecast().framasVN()
        print("Previous modified: ",a)
        print("Last modified:     ",b)

        if a != b:
            print("\nFound a newly uploaded file !")
            print("Please wait to import data from the new file into the database !\n")

            actual_forecast().framasVN()
            create_new_modify_time_actual_forecast().framasVN()
            print('framasVN data has been imported into database !\n')
        else:
            print('No new files have been uploaded !\n')

        return

class budget_type():
    def __init__(self):
        pass

    def framasDE(self):
        print("\n____framasDE____")

        a = previous_modified_budget().framasDE()
        b = last_modified_budget().framasDE()
        print("Previous modified: ",a)
        print("Last modified:     ",b)

        if a != b:
            print("\nFound a newly uploaded file !")
            print("Please wait to import data from the new file into the database !\n")

            budget().framasDE()
            create_new_modify_time_budget().framasDE()
            print('framasDE data has been imported into database !\n')
        else:
            print('No new files have been uploaded !\n')

        return

    def framasFZ(self):
        print("\n____framasFZ____")

        a = previous_modified_budget().framasFZ()
        b = last_modified_budget().framasFZ()
        print("Previous modified: ",a)
        print("Last modified:     ",b)

        if a != b:
            print("\nFound a newly uploaded file !")
            print("Please wait to import data from the new file into the database !\n")

            budget().framasFZ()
            create_new_modify_time_budget().framasFZ()
            print('framasFZ data has been imported into database !\n')
        else:
            print('No new files have been uploaded !\n')

        return
        
    def framasHN(self):
        print("\n____framasHN____")
        
        a = previous_modified_budget().framasHN()
        b = last_modified_budget().framasHN()
        print("Previous modified: ",a)
        print("Last modified:     ",b)

        if a != b:
            print("\nFound a newly uploaded file !")
            print("Please wait to import data from the new file into the database !\n")

            budget().framasHN()
            create_new_modify_time_budget().framasHN()
            print('framasHN data has been imported into database !\n')
        else:
            print('No new files have been uploaded !\n')

        return
        
    def framasID(self):
        print("\n____framasID____")

        a = previous_modified_budget().framasID()
        b = last_modified_budget().framasID()
        print("Previous modified: ",a)
        print("Last modified:     ",b)

        if a != b:
            print("\nFound a newly uploaded file !")
            print("Please wait to import data from the new file into the database !\n")

            budget().framasID()
            create_new_modify_time_budget().framasID()
            print('framasID data has been imported into database !\n')
        else:
            print('No new files have been uploaded !\n')

        return
        
    def framasKR(self):
        print("\n____framasKR____")

        a = previous_modified_budget().framasKR()
        b = last_modified_budget().framasKR()
        print("Previous modified: ",a)
        print("Last modified:     ",b)

        if a != b:
            print("\nFound a newly uploaded file !")
            print("Please wait to import data from the new file into the database !\n")

            budget().framasKR()
            create_new_modify_time_budget().framasKR()
            print('framasKR data has been imported into database !\n')
        else:
            print('No new files have been uploaded !\n')

        return
        
    def framasKV(self):
        print("\n____framasKV____")

        a = previous_modified_budget().framasKV()
        b = last_modified_budget().framasKV()
        print("Previous modified: ",a)
        print("Last modified:     ",b)

        if a != b:
            print("\nFound a newly uploaded file !")
            print("Please wait to import data from the new file into the database !\n")

            budget().framasKV()
            create_new_modify_time_budget().framasKV()
            print('framasKV data has been imported into database !\n')
        else:
            print('No new files have been uploaded !\n')

        return
        
    def framasVN(self):
        print("\n____framasVN____")

        a = previous_modified_budget().framasVN()
        b = last_modified_budget().framasVN()
        print("Previous modified: ",a)
        print("Last modified:     ",b)

        if a != b:
            print("\nFound a newly uploaded file !")
            print("Please wait to import data from the new file into the database !\n")

            budget().framasVN()
            create_new_modify_time_budget().framasVN()
            print('framasVN data has been imported into database !\n')
        else:
            print('No new files have been uploaded !\n')

        return

class run_all():
    def __init__(self):
        pass

    def my_function(self):
        # try:
        #     print('\n____________| Monthly |____________')
        #     actual_forecast_type().framasDE()
        # except KeyError:
        #     print("No excel file found, please check again !")
        # except Exception:
        #     print("An error occurred !")
        
        # try:
        #     actual_forecast_type().framasFZ()
        # except KeyError:
        #     print("No excel file found, please check again !")
        # except Exception:
        #     print("An error occurred !")

        # try:
        #     actual_forecast_type().framasHN()
        # except KeyError:
        #     print("No excel file found, please check again !")
        # except Exception:
        #     print("An error occurred !")

        # try:
        #     actual_forecast_type().framasID()
        # except KeyError:
        #     print("No excel file found, please check again !")
        # except Exception:
        #     print("An error occurred !")

        # try:
        #     actual_forecast_type().framasKR()
        # except KeyError:
        #     print("No excel file found, please check again !")
        # except Exception:
        #     print("An error occurred !")

        # try:
        #     actual_forecast_type().framasKV()
        # except KeyError:
        #     print("No excel file found, please check again !")
        # except Exception:
        #     print("An error occurred !")

        # try:
        #     actual_forecast_type().framasVN()
        # except KeyError:
        #     print("No excel file found, please check again !")
        # except Exception:
        #     print("An error occurred !")

        
        print('\n____________| Budget |____________')
        
        try:
            budget_type().framasDE()
        except KeyError:
            print("No excel file found, please check again !")
        except Exception:
            print("An error occurred !")

        try:
            budget_type().framasFZ()
        except KeyError:
            print("No excel file found, please check again !")
        except Exception:
            print("An error occurred !")

        try:
            budget_type().framasHN()
        except KeyError:
            print("No excel file found, please check again !")
        except Exception:
            print("An error occurred !")

        try:
            budget_type().framasID()
        except KeyError:
            print("No excel file found, please check again !")
        except Exception:
            print("An error occurred !")

        try:
            budget_type().framasKR()
        except KeyError:
            print("No excel file found, please check again !")
        except Exception:
            print("An error occurred !")

        try:
            budget_type().framasKV()
        except KeyError:
            print("No excel file found, please check again !")
        except Exception:
            print("An error occurred !")

        try:
            budget_type().framasVN()
        except KeyError:
            print("No excel file found, please check again !")
        except Exception:
            print("An error occurred !")

        cursor.close()
        conn.close()

        print("Done !")
        time.sleep(5)

        return

conn = connect().sql_server()
cursor = conn.cursor()

if __name__ == "__main__":
    run_all().my_function()

