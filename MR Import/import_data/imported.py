import os
import sys
sys.path.append(os.path.realpath('.'))
from connect.database import connect
from dataframe.actual_forecast_dataframe import total_data_frame
from dataframe.budget_dataframe import budget_dataframe
from info.excel_file import actual_scenario_id, budget_scenario_id, forecast_scenario_id
from info.excel_file import facID_framasDE, facID_framasFZ, facID_framasHN, facID_framasID, facID_framasKR, facID_framasKV, facID_framasVN

class actual_forecast():
    def __init__(self):
        pass

    def framasDE(self):
        df = total_data_frame().framasDE()

        cursor.execute('''  
                        DELETE FROM TBL_MR_SALES 
                        WHERE (MRS_FACTORY_ID = %s AND MRS_SCENARIO_ID = %s) 
                        OR (MRS_FACTORY_ID = %s AND MRS_SCENARIO_ID = %s)
                        ''', 
                            (facID_framasDE,
                            actual_scenario_id,
                            facID_framasDE,
                            forecast_scenario_id))
        
        for index,row in df.iterrows():
            cursor.execute('''  
                                INSERT INTO TBL_MR_SALES([MRS_FACTORY_ID], [MRS_INDEX], [MRS_PROJECT_NO], [MRS_SALE_DATE], [MRS_DATA_TYPE], [MRS_SALE_VALUE], [MRS_ROLLING_DATE], [MRS_SCENARIO_ID]) 
                                VALUES (%s,%s,%s,%s,%s,%s,%s,%s)
                            ''', 
                                (row['MRS_FACTORY_ID'],
                                row['MRS_INDEX'],
                                row['MRS_PROJECT_NO'],
                                row['MRS_SALE_DATE'],
                                row['MRS_DATA_TYPE'],
                                row['MRS_SALE_VALUE'],
                                row['MRS_ROLLING_DATE'],
                                row['MRS_SCENARIO_ID']))

            print('Insert data: ',' framasDE ',row['MRS_SALE_DATE'],row['MRS_SALE_VALUE'],' ...')

        conn.commit()

        print('\nDATAFRAME: ')
        print(df)

        return df

    def framasFZ(self):
        df = total_data_frame().framasFZ()

        cursor.execute('''  
                        DELETE FROM TBL_MR_SALES 
                        WHERE (MRS_FACTORY_ID = %s AND MRS_SCENARIO_ID = %s) 
                        OR (MRS_FACTORY_ID = %s AND MRS_SCENARIO_ID = %s)
                        ''', 
                            (facID_framasFZ,
                            actual_scenario_id,
                            facID_framasFZ,
                            forecast_scenario_id))
        
        for index,row in df.iterrows():
            cursor.execute('''  
                                INSERT INTO TBL_MR_SALES([MRS_FACTORY_ID], [MRS_INDEX], [MRS_PROJECT_NO], [MRS_SALE_DATE], [MRS_DATA_TYPE], [MRS_SALE_VALUE], [MRS_ROLLING_DATE], [MRS_SCENARIO_ID]) 
                                VALUES (%s,%s,%s,%s,%s,%s,%s,%s)
                            ''', 
                                (row['MRS_FACTORY_ID'],
                                row['MRS_INDEX'],
                                row['MRS_PROJECT_NO'],
                                row['MRS_SALE_DATE'],
                                row['MRS_DATA_TYPE'],
                                row['MRS_SALE_VALUE'],
                                row['MRS_ROLLING_DATE'],
                                row['MRS_SCENARIO_ID']))

            print('Insert data: ',' framasFZ ',row['MRS_SALE_DATE'],row['MRS_SALE_VALUE'],' ...')

        conn.commit()

        print('\nDATAFRAME: ')
        print(df)

        return df

    def framasHN(self):
        df = total_data_frame().framasHN()

        cursor.execute('''  
                        DELETE FROM TBL_MR_SALES 
                        WHERE (MRS_FACTORY_ID = %s AND MRS_SCENARIO_ID = %s) 
                        OR (MRS_FACTORY_ID = %s AND MRS_SCENARIO_ID = %s)
                        ''', 
                            (facID_framasHN,
                            actual_scenario_id,
                            facID_framasHN,
                            forecast_scenario_id))
        
        for index,row in df.iterrows():
            cursor.execute('''  
                                INSERT INTO TBL_MR_SALES([MRS_FACTORY_ID], [MRS_INDEX], [MRS_PROJECT_NO], [MRS_SALE_DATE], [MRS_DATA_TYPE], [MRS_SALE_VALUE], [MRS_ROLLING_DATE], [MRS_SCENARIO_ID]) 
                                VALUES (%s,%s,%s,%s,%s,%s,%s,%s)
                            ''', 
                                (row['MRS_FACTORY_ID'],
                                row['MRS_INDEX'],
                                row['MRS_PROJECT_NO'],
                                row['MRS_SALE_DATE'],
                                row['MRS_DATA_TYPE'],
                                row['MRS_SALE_VALUE'],
                                row['MRS_ROLLING_DATE'],
                                row['MRS_SCENARIO_ID']))

            print('Insert data: ',' framasHN ',row['MRS_SALE_DATE'],row['MRS_SALE_VALUE'],' ...')

        conn.commit()
        
        print('\nDATAFRAME: ')
        print(df)

        return df

    def framasID(self):
        df = total_data_frame().framasID()

        cursor.execute('''  
                        DELETE FROM TBL_MR_SALES 
                        WHERE (MRS_FACTORY_ID = %s AND MRS_SCENARIO_ID = %s) 
                        OR (MRS_FACTORY_ID = %s AND MRS_SCENARIO_ID = %s)
                        ''', 
                            (facID_framasID,
                            actual_scenario_id,
                            facID_framasID,
                            forecast_scenario_id))
        
        for index,row in df.iterrows():
            cursor.execute('''  
                                INSERT INTO TBL_MR_SALES([MRS_FACTORY_ID], [MRS_INDEX], [MRS_PROJECT_NO], [MRS_SALE_DATE], [MRS_DATA_TYPE], [MRS_SALE_VALUE], [MRS_ROLLING_DATE], [MRS_SCENARIO_ID]) 
                                VALUES (%s,%s,%s,%s,%s,%s,%s,%s)
                            ''', 
                                (row['MRS_FACTORY_ID'],
                                row['MRS_INDEX'],
                                row['MRS_PROJECT_NO'],
                                row['MRS_SALE_DATE'],
                                row['MRS_DATA_TYPE'],
                                row['MRS_SALE_VALUE'],
                                row['MRS_ROLLING_DATE'],
                                row['MRS_SCENARIO_ID']))

            print('Insert data: ',' framasID ',row['MRS_SALE_DATE'],row['MRS_SALE_VALUE'],' ...')

        conn.commit()

        print('\nDATAFRAME: ')
        print(df)

        return df

    def framasKR(self):
        df = total_data_frame().framasKR()

        cursor.execute('''  
                        DELETE FROM TBL_MR_SALES 
                        WHERE (MRS_FACTORY_ID = %s AND MRS_SCENARIO_ID = %s) 
                        OR (MRS_FACTORY_ID = %s AND MRS_SCENARIO_ID = %s)
                        ''', 
                            (facID_framasKR,
                            actual_scenario_id,
                            facID_framasKR,
                            forecast_scenario_id))
        
        for index,row in df.iterrows():
            cursor.execute('''  
                                INSERT INTO TBL_MR_SALES([MRS_FACTORY_ID], [MRS_INDEX], [MRS_PROJECT_NO], [MRS_SALE_DATE], [MRS_DATA_TYPE], [MRS_SALE_VALUE], [MRS_ROLLING_DATE], [MRS_SCENARIO_ID]) 
                                VALUES (%s,%s,%s,%s,%s,%s,%s,%s)
                            ''', 
                                (row['MRS_FACTORY_ID'],
                                row['MRS_INDEX'],
                                row['MRS_PROJECT_NO'],
                                row['MRS_SALE_DATE'],
                                row['MRS_DATA_TYPE'],
                                row['MRS_SALE_VALUE'],
                                row['MRS_ROLLING_DATE'],
                                row['MRS_SCENARIO_ID']))

            print('Insert data: ',' framasKR ',row['MRS_SALE_DATE'],row['MRS_SALE_VALUE'],' ...')

        conn.commit()

        print('\nDATAFRAME: ')
        print(df)

        return df

    def framasKV(self):
        df = total_data_frame().framasKV()

        cursor.execute('''  
                        DELETE FROM TBL_MR_SALES 
                        WHERE (MRS_FACTORY_ID = %s AND MRS_SCENARIO_ID = %s) 
                        OR (MRS_FACTORY_ID = %s AND MRS_SCENARIO_ID = %s)
                        ''', 
                            (facID_framasKV,
                            actual_scenario_id,
                            facID_framasKV,
                            forecast_scenario_id))
        
        for index,row in df.iterrows():
            cursor.execute('''  
                                INSERT INTO TBL_MR_SALES([MRS_FACTORY_ID], [MRS_INDEX], [MRS_PROJECT_NO], [MRS_SALE_DATE], [MRS_DATA_TYPE], [MRS_SALE_VALUE], [MRS_ROLLING_DATE], [MRS_SCENARIO_ID]) 
                                VALUES (%s,%s,%s,%s,%s,%s,%s,%s)
                            ''', 
                                (row['MRS_FACTORY_ID'],
                                row['MRS_INDEX'],
                                row['MRS_PROJECT_NO'],
                                row['MRS_SALE_DATE'],
                                row['MRS_DATA_TYPE'],
                                row['MRS_SALE_VALUE'],
                                row['MRS_ROLLING_DATE'],
                                row['MRS_SCENARIO_ID']))

            print('Insert data: ',' framasKV ',row['MRS_SALE_DATE'],row['MRS_SALE_VALUE'],' ...')

        conn.commit()

        print('\nDATAFRAME: ')
        print(df)

        return df

    def framasVN(self):
        df = total_data_frame().framasVN()

        cursor.execute('''  
                        DELETE FROM TBL_MR_SALES 
                        WHERE (MRS_FACTORY_ID = %s AND MRS_SCENARIO_ID = %s) 
                        OR (MRS_FACTORY_ID = %s AND MRS_SCENARIO_ID = %s)
                        ''', 
                            (facID_framasVN,
                            actual_scenario_id,
                            facID_framasVN,
                            forecast_scenario_id))
        
        for index,row in df.iterrows():
            cursor.execute('''  
                                INSERT INTO TBL_MR_SALES([MRS_FACTORY_ID], [MRS_INDEX], [MRS_PROJECT_NO], [MRS_SALE_DATE], [MRS_DATA_TYPE], [MRS_SALE_VALUE], [MRS_ROLLING_DATE], [MRS_SCENARIO_ID]) 
                                VALUES (%s,%s,%s,%s,%s,%s,%s,%s)
                            ''', 
                                (row['MRS_FACTORY_ID'],
                                row['MRS_INDEX'],
                                row['MRS_PROJECT_NO'],
                                row['MRS_SALE_DATE'],
                                row['MRS_DATA_TYPE'],
                                row['MRS_SALE_VALUE'],
                                row['MRS_ROLLING_DATE'],
                                row['MRS_SCENARIO_ID']))

            print('Insert data: ',' framasVN ',row['MRS_SALE_DATE'],row['MRS_SALE_VALUE'],' ...')

        conn.commit()

        print('\nDATAFRAME: ')
        print(df)

        return df

class budget():
    def __init__(self):
        pass

    def framasDE(self):
        df = budget_dataframe().framasDE()

        cursor.execute('''  
                        DELETE FROM TBL_MR_SALES 
                        WHERE MRS_FACTORY_ID = %s
                        AND MRS_SCENARIO_ID = %s
                        ''', 
                            (facID_framasDE, budget_scenario_id))

        for index,row in df.iterrows():
            cursor.execute('''  
                                INSERT INTO TBL_MR_SALES([MRS_FACTORY_ID], [MRS_INDEX], [MRS_PROJECT_NO], [MRS_SALE_DATE], [MRS_DATA_TYPE], [MRS_SALE_VALUE], [MRS_ROLLING_DATE], [MRS_SCENARIO_ID]) 
                                VALUES (%s,%s,%s,%s,%s,%s,%s,%s)
                            ''', 
                                (row['MRS_FACTORY_ID'],
                                row['MRS_INDEX'],
                                row['MRS_PROJECT_NO'],
                                row['MRS_SALE_DATE'],
                                row['MRS_DATA_TYPE'],
                                row['MRS_SALE_VALUE'],
                                row['MRS_ROLLING_DATE'],
                                row['MRS_SCENARIO_ID']))

            print('Insert data: ',' framasDE ',row['MRS_SALE_DATE'],row['MRS_SALE_VALUE'],' ...')

        conn.commit()

        print('\nDATAFRAME: ')
        print(df)

        return df

    def framasFZ(self):
        df = budget_dataframe().framasFZ()

        cursor.execute('''  
                        DELETE FROM TBL_MR_SALES 
                        WHERE MRS_FACTORY_ID = %s
                        AND MRS_SCENARIO_ID = %s
                        ''', 
                            (facID_framasFZ, budget_scenario_id))

        for index,row in df.iterrows():
            cursor.execute('''  
                                INSERT INTO TBL_MR_SALES([MRS_FACTORY_ID], [MRS_INDEX], [MRS_PROJECT_NO], [MRS_SALE_DATE], [MRS_DATA_TYPE], [MRS_SALE_VALUE], [MRS_ROLLING_DATE], [MRS_SCENARIO_ID]) 
                                VALUES (%s,%s,%s,%s,%s,%s,%s,%s)
                            ''', 
                                (row['MRS_FACTORY_ID'],
                                row['MRS_INDEX'],
                                row['MRS_PROJECT_NO'],
                                row['MRS_SALE_DATE'],
                                row['MRS_DATA_TYPE'],
                                row['MRS_SALE_VALUE'],
                                row['MRS_ROLLING_DATE'],
                                row['MRS_SCENARIO_ID']))

            print('Insert data: ',' framasFZ ',row['MRS_SALE_DATE'],row['MRS_SALE_VALUE'],' ...')

        conn.commit()

        print('\nDATAFRAME: ')
        print(df)

        return df

    def framasHN(self):
        df = budget_dataframe().framasHN()

        cursor.execute('''  
                        DELETE FROM TBL_MR_SALES 
                        WHERE MRS_FACTORY_ID = %s
                        AND MRS_SCENARIO_ID = %s
                        ''', 
                            (facID_framasHN, budget_scenario_id))

        for index,row in df.iterrows():
            cursor.execute('''  
                                INSERT INTO TBL_MR_SALES([MRS_FACTORY_ID], [MRS_INDEX], [MRS_PROJECT_NO], [MRS_SALE_DATE], [MRS_DATA_TYPE], [MRS_SALE_VALUE], [MRS_ROLLING_DATE], [MRS_SCENARIO_ID]) 
                                VALUES (%s,%s,%s,%s,%s,%s,%s,%s)
                            ''', 
                                (row['MRS_FACTORY_ID'],
                                row['MRS_INDEX'],
                                row['MRS_PROJECT_NO'],
                                row['MRS_SALE_DATE'],
                                row['MRS_DATA_TYPE'],
                                row['MRS_SALE_VALUE'],
                                row['MRS_ROLLING_DATE'],
                                row['MRS_SCENARIO_ID']))

            print('Insert data: ',' framasHN ',row['MRS_SALE_DATE'],row['MRS_SALE_VALUE'],' ...')

        conn.commit()

        print('\nDATAFRAME: ')
        print(df)

        return df

    def framasID(self):
        df = budget_dataframe().framasID()

        cursor.execute('''  
                        DELETE FROM TBL_MR_SALES 
                        WHERE MRS_FACTORY_ID = %s
                        AND MRS_SCENARIO_ID = %s
                        ''', 
                            (facID_framasID, budget_scenario_id))

        for index,row in df.iterrows():
            cursor.execute('''  
                                INSERT INTO TBL_MR_SALES([MRS_FACTORY_ID], [MRS_INDEX], [MRS_PROJECT_NO], [MRS_SALE_DATE], [MRS_DATA_TYPE], [MRS_SALE_VALUE], [MRS_ROLLING_DATE], [MRS_SCENARIO_ID]) 
                                VALUES (%s,%s,%s,%s,%s,%s,%s,%s)
                            ''', 
                                (row['MRS_FACTORY_ID'],
                                row['MRS_INDEX'],
                                row['MRS_PROJECT_NO'],
                                row['MRS_SALE_DATE'],
                                row['MRS_DATA_TYPE'],
                                row['MRS_SALE_VALUE'],
                                row['MRS_ROLLING_DATE'],
                                row['MRS_SCENARIO_ID']))

            print('Insert data: ',' framasID ',row['MRS_SALE_DATE'],row['MRS_SALE_VALUE'],' ...')

        conn.commit()

        print('\nDATAFRAME: ')
        print(df)

        return df

    def framasKR(self):
        df = budget_dataframe().framasKR()

        cursor.execute('''  
                        DELETE FROM TBL_MR_SALES 
                        WHERE MRS_FACTORY_ID = %s
                        AND MRS_SCENARIO_ID = %s
                        ''', 
                            (facID_framasKR, budget_scenario_id))

        for index,row in df.iterrows():
            cursor.execute('''  
                                INSERT INTO TBL_MR_SALES([MRS_FACTORY_ID], [MRS_INDEX], [MRS_PROJECT_NO], [MRS_SALE_DATE], [MRS_DATA_TYPE], [MRS_SALE_VALUE], [MRS_ROLLING_DATE], [MRS_SCENARIO_ID]) 
                                VALUES (%s,%s,%s,%s,%s,%s,%s,%s)
                            ''', 
                                (row['MRS_FACTORY_ID'],
                                row['MRS_INDEX'],
                                row['MRS_PROJECT_NO'],
                                row['MRS_SALE_DATE'],
                                row['MRS_DATA_TYPE'],
                                row['MRS_SALE_VALUE'],
                                row['MRS_ROLLING_DATE'],
                                row['MRS_SCENARIO_ID']))

            print('Insert data: ',' framasKR ',row['MRS_SALE_DATE'],row['MRS_SALE_VALUE'],' ...')

        conn.commit()

        print('\nDATAFRAME: ')
        print(df)

        return df

    def framasKV(self):
        df = budget_dataframe().framasKV()

        cursor.execute('''  
                        DELETE FROM TBL_MR_SALES 
                        WHERE MRS_FACTORY_ID = %s
                        AND MRS_SCENARIO_ID = %s
                        ''', 
                            (facID_framasKV, budget_scenario_id))

        for index,row in df.iterrows():
            cursor.execute('''  
                                INSERT INTO TBL_MR_SALES([MRS_FACTORY_ID], [MRS_INDEX], [MRS_PROJECT_NO], [MRS_SALE_DATE], [MRS_DATA_TYPE], [MRS_SALE_VALUE], [MRS_ROLLING_DATE], [MRS_SCENARIO_ID]) 
                                VALUES (%s,%s,%s,%s,%s,%s,%s,%s)
                            ''', 
                                (row['MRS_FACTORY_ID'],
                                row['MRS_INDEX'],
                                row['MRS_PROJECT_NO'],
                                row['MRS_SALE_DATE'],
                                row['MRS_DATA_TYPE'],
                                row['MRS_SALE_VALUE'],
                                row['MRS_ROLLING_DATE'],
                                row['MRS_SCENARIO_ID']))

            print('Insert data: ',' framasKV ',row['MRS_SALE_DATE'],row['MRS_SALE_VALUE'],' ...')

        conn.commit()

        print('\nDATAFRAME: ')
        print(df)

        return df

    def framasVN(self):
        df = budget_dataframe().framasVN()

        cursor.execute('''  
                        DELETE FROM TBL_MR_SALES 
                        WHERE MRS_FACTORY_ID = %s
                        AND MRS_SCENARIO_ID = %s
                        ''', 
                            (facID_framasVN, budget_scenario_id))

        for index,row in df.iterrows():
            cursor.execute('''  
                                INSERT INTO TBL_MR_SALES([MRS_FACTORY_ID], [MRS_INDEX], [MRS_PROJECT_NO], [MRS_SALE_DATE], [MRS_DATA_TYPE], [MRS_SALE_VALUE], [MRS_ROLLING_DATE], [MRS_SCENARIO_ID]) 
                                VALUES (%s,%s,%s,%s,%s,%s,%s,%s)
                            ''', 
                                (row['MRS_FACTORY_ID'],
                                row['MRS_INDEX'],
                                row['MRS_PROJECT_NO'],
                                row['MRS_SALE_DATE'],
                                row['MRS_DATA_TYPE'],
                                row['MRS_SALE_VALUE'],
                                row['MRS_ROLLING_DATE'],
                                row['MRS_SCENARIO_ID']))

            print('Insert data: ',' framasVN ',row['MRS_SALE_DATE'],row['MRS_SALE_VALUE'],' ...')

        print('\nDATAFRAME: ')
        print(df)

        conn.commit()

        return df    

conn = connect().sql_server()
cursor = conn.cursor()