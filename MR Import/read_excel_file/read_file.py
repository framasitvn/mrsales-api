import os
import sys
sys.path.append(os.path.realpath('.'))
import pandas as pd
from info.sharepoint import sites_name, folder, folder_budget, file_name, file_name_budget
from info.excel_file import sheet_name, engine
from connect.sharepoint import connect
from shareplum.office365 import Office365

class read_file():

    def __init__(self):
        pass

    def framasDE(self):

        folders = site.Folder(folder().framasDE())
        file = folders.get_file(file_name().framasDE())

        df = pd.read_excel(file, sheet_name=sheet_name, engine=engine)

        return df

    def framasFZ(self):

        folders = site.Folder(folder().framasFZ())
        file = folders.get_file(file_name().framasFZ())

        df = pd.read_excel(file, sheet_name=sheet_name, engine=engine)

        return df

    def framasHN(self):

        folders = site.Folder(folder().framasHN())
        file = folders.get_file(file_name().framasHN())

        df = pd.read_excel(file, sheet_name=sheet_name, engine=engine)

        return df

    def framasID(self):

        folders = site.Folder(folder().framasID())
        file = folders.get_file(file_name().framasID())

        df = pd.read_excel(file, sheet_name=sheet_name, engine=engine)

        return df

    def framasKR(self):

            folders = site.Folder(folder().framasKR())
            file = folders.get_file(file_name().framasKR())

            df = pd.read_excel(file, sheet_name=sheet_name, engine=engine)

            return df

    def framasKV(self):

        folders = site.Folder(folder().framasKV())
        file = folders.get_file(file_name().framasKV())

        df = pd.read_excel(file, sheet_name=sheet_name, engine=engine)

        return df

    def framasVN(self):

        folders = site.Folder(folder().framasVN())
        file = folders.get_file(file_name().framasVN())

        df = pd.read_excel(file, sheet_name=sheet_name, engine=engine)

        return df

class read_file_budget():

    def __init__(self):
        pass

    def framasDE(self):

        folders = site.Folder(folder_budget().framasDE())
        file = folders.get_file(file_name_budget().framasDE())

        df = pd.read_excel(file, sheet_name=sheet_name, engine=engine)

        return df

    def framasFZ(self):

        folders = site.Folder(folder_budget().framasFZ())
        file = folders.get_file(file_name_budget().framasFZ())

        df = pd.read_excel(file, sheet_name=sheet_name, engine=engine)

        return df

    def framasHN(self):

        folders = site.Folder(folder_budget().framasHN())
        file = folders.get_file(file_name_budget().framasHN())

        df = pd.read_excel(file, sheet_name=sheet_name, engine=engine)

        return df

    def framasID(self):

        folders = site.Folder(folder_budget().framasID())
        file = folders.get_file(file_name_budget().framasID())

        df = pd.read_excel(file, sheet_name=sheet_name, engine=engine)

        return df

    def framasKR(self):

            folders = site.Folder(folder_budget().framasKR())
            file = folders.get_file(file_name_budget().framasKR())

            df = pd.read_excel(file, sheet_name=sheet_name, engine=engine)

            return df

    def framasKV(self):

        folders = site.Folder(folder_budget().framasKV())
        file = folders.get_file(file_name_budget().framasKV())

        df = pd.read_excel(file, sheet_name=sheet_name, engine=engine)

        return df

    def framasVN(self):

        folders = site.Folder(folder_budget().framasVN())
        file = folders.get_file(file_name_budget().framasVN())

        df = pd.read_excel(file, sheet_name=sheet_name, engine=engine)

        return df

site = connect().sharepoint()
